import pytest
from httpx import AsyncClient
from pydantic import ValidationError
from sqlalchemy import select
from sqlalchemy.testing import db
from starlette.testclient import TestClient

from src import models
from src.app import app
from src.database import session, engine
from src.models import Recipe, AllRecipes

client = TestClient(app)


@pytest.mark.asyncio
async def test_add_recipe():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
        await session.commit()

        data = {
            "title": "Борщ",
            "time": 60,
            "ingredients": "Свекла",
            "description": "Добавить сметану"
        }
        async with AsyncClient(app=app, base_url="http://test") as client:
            try:
                response = await client.post("/add_recipe/", json=data)
            except ValidationError:
                pass

            res = await session.execute(select(models.AllRecipes).filter_by(title="Борщ"))
            recipes = res.fetchall()
            assert len(recipes) > 0


@pytest.mark.asyncio
async def test_get_recipies():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
        await session.commit()
    response = client.get("/recipes")
    print(response.text)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_recipies_by_id():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
        await session.commit()
    new_recipe = models.AllRecipes(
        id=5,
        title="Омлет",
        time=10,
        ingredients="Яйца",
        description="На сковороде готовить"
    )
    session.add(new_recipe)
    await session.commit()
    response = client.get("/recipes")
    res = await session.execute(select(models.AllRecipes).filter_by(id=5))
    recipes = res.fetchall()
    assert len(recipes) > 0
